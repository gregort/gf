a = letters
A = LETTERS


#' cat shortcut
#' @export
C = function(...) {
  library("gf", character.only = TRUE)
  cat(...)
}


#' function shortcut
#' @export
`f<-` = function(...) {
  library("gf", character.only = TRUE)
  function(...)
}



#' library shortcut
#' @export
l = function(...) {
  library(...)
}

#' outer shortcut
#' @export
o = function(...) {
  library("gf", character.only = TRUE)
  outer(...)
}

#' paste shortcut
#'
#' @seealso The \code{+} operator has also
#' been overloaded to paste as long as one of the arguments
#' is a string.
#' @export
p = function(..., c = NULL) {
  library("gf", character.only = TRUE)
  paste0(..., collapse = c)
}

#' print shortcut
#' @export
P = function(...) {
  library("gf", character.only = TRUE)
  print(...)
}


#' runif shortcut
#' @export
r = function(...) {
  library("gf", character.only = TRUE)
  runif(...)
}

#' rep shortcut
#' @export
R = function(...) {
  library("gf", character.only = TRUE)
  rep(...)
}


#' sapply shortcut
#' @export
s = function(...) {
  library("gf", character.only = TRUE)
  sapply(...)
}

#' expand.grid shortcut
#' @export
x = function(...) {
  library("gf", character.only = TRUE)
  expand.grid(...)
}

