#' + operator overloaded to concatenate strings
#' @export
`+` = function(e1, e2) {
  if (is.character(e1) | is.character(e2)) {
    return(paste0(e1, e2))
  }
  .Primitive("+")(e1, e2)
}
